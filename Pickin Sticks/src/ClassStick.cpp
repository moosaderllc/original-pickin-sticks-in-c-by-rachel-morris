#include "ClassStick.h"
#include <cstdlib>

Stick::Stick() { x = 32; y = 32; }

Stick::~Stick() { }

int Stick::X() { return x; }

int Stick::Y() { return y; }

void Stick::generateNewCoordinates()
{
  int tempX;
  int tempY;

  tempX = ( (int)rand() % 18 + 1 ) * 32;
  tempY = ( (int)rand() % 14 + 1 ) * 32;

  x = tempX;
  y = tempY;

}