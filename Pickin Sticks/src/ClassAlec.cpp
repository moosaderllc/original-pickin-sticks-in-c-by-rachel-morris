#include "ClassAlec.h"

Alec::Alec() { speed = 1.0; direction = 2; iTotalPicked = 0; x = 640/2 - 32/2; y = 480/2 - 32/2; }

Alec::~Alec() { }

int Alec::X() { return x; }

int Alec::Y() { return y; }

int Alec::Direction() { return direction; }

int Alec::TotalPicked() { return iTotalPicked; }

void Alec::incrementTotalPicked() { iTotalPicked++; }

void Alec::move(int dir)
{
  if (dir == 2) //Down.  Look at Numpad to figure it out.
  {
    if (y < 448) { y += speed; }
    direction = 2;
  }
  else if (dir == 4)
  {
    if (x > 0) { x -= speed; }
    direction = 4;
  }
  else if (dir == 6)
  {
    if (x < 608) { x += speed; }
    direction = 6;
  }
  else if (dir == 8)
  {
    if (y > 10) { y -= speed; }
    direction = 8;
  }
}

void Alec::increaseSpeed()
{
    if ( speed < 100 ) { speed += 1.0; }
}

void Alec::draw(BITMAP *buffer, BITMAP *bmpAlec)
{
    //Ghosty trail for when you're moving really fast :O
        int tx = x;
        int ty = y;
        int tx2 = x;
        int ty2 = y;
        int tx3 = x;
        int ty3 = y;
        BITMAP *temp = create_bitmap(64, 64);
        rectfill(temp, 0, 0, 64, 64, makecol(255, 0, 255));
//        for (int i=0; i<(speed % 10); i++)
//        {
            int i=speed;
            if (Direction() == 2)
            {
                    ty -= speed*i*2;
                    ty2 -= speed*i*4;
                    ty3 -= speed*i*6;
                masked_blit(bmpAlec, temp, 0,0, 0, 0,64,64);
            }
            else if (Direction() == 4)
            {
                    tx += speed*i*2;
                    tx2 += speed*i*4;
                    tx3 += speed*i*6;
                masked_blit(bmpAlec, temp, 32*2,0, 0, 0,64,64);
            }
            else if (Direction() == 6)
            {
                    tx -= speed*i*2;
                    tx2 -= speed*i*4;
                    tx3 -= speed*i*6;
                masked_blit(bmpAlec, temp, 64*2,0, 0, 0,64,64);
            }
            else if (Direction() == 8)
            {
                    ty += speed*i*2;
                    ty2 += speed*i*4;
                    ty3 += speed*i*6;
                masked_blit(bmpAlec, temp, 96*2,0, 0, 0,64,64);
            }
            drawing_mode(DRAW_MODE_TRANS, 0, 0, 0);
            set_trans_blender(0, 0, 0, 128);
            if ( speed > 6 ) { draw_trans_sprite(buffer, temp, tx3, ty3); }
            if ( speed > 4 ) { draw_trans_sprite(buffer, temp, tx2, ty2); }
            if ( speed > 2 ) { draw_trans_sprite(buffer, temp, tx, ty); }
            drawing_mode(DRAW_MODE_SOLID, 0, 0, 0);
//        }

    if (Direction() == 2)      { masked_blit(bmpAlec, buffer, 0,0, x, y,64,64); }
    else if (Direction() == 4) { masked_blit(bmpAlec, buffer, 32*2,0, x, y,64,64); }
    else if (Direction() == 6) { masked_blit(bmpAlec, buffer, 64*2,0, x, y,64,64); }
    else if (Direction() == 8) { masked_blit(bmpAlec, buffer, 96*2,0, x, y,64,64); }
}

