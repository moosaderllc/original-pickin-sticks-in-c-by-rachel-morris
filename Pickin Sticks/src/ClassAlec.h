#pragma once
#include <allegro.h>

class Alec
{
  private:
  int iTotalPicked;
  int x, y, direction;
  float speed;
  int prev_x, prev_y, prev_dir;
  public:
  void incrementTotalPicked();
  void move(int);
  void increaseSpeed();
  void draw(BITMAP *buffer, BITMAP *image);
  void update_prev() { prev_x = x; prev_y = y; prev_dir = direction; }
  float Speed() { return speed; }
  int X();
  int Y();
  int TotalPicked();
  int Direction();
  Alec();
  ~Alec();
};
